﻿using System;
using System.Xml;

namespace LogicCalc
{
    public enum OperationKind
    {
        And,
        Or,
        Xor,
        Not
    }


    public class Entry
    {
        Boolean x;
        Boolean y;
        Boolean result;
        OperationKind operation;

        public Entry(Boolean x, OperationKind operation, Boolean y, Boolean result)
        {
            this.x = x;
            this.operation = operation;
            this.y = y;
            this.result = result;
        }

        public override string ToString()
        {
            if (operation == OperationKind.Not)
            {
                return string.Format("{0} {1} = {2}", operation, x, result);
            }
            else
            {
                return $"{x} {operation} {y} = {result}";
            }
        }

        public Boolean X
        {
            get { return x; }
        }

        public Boolean Y
        {
            get { return y; }
        }

        public OperationKind Operation
        {
            get { return operation; }
        }

        public Boolean Result
        {
            get { return result; }
        }

    }
}
