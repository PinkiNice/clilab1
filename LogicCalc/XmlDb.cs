﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.IO;
namespace LogicCalc
{
    class XmlDb
    {
        private string _path ;
        private XDocument _dataBase = new XDocument();

        public XmlDb(string path)
        {
            _path = path;
            _dataBase = XDocument.Load(path);
        }

        public void Save(string path)
        {
            _dataBase.Save(path);
        }

        public void Add(XElement element)
        {
            _dataBase.Add(element);
        }

        public XDocument getAll()
        {
            return _dataBase;
        }

        public static void MakeDbFile(string path)
        {
            File.Create(path).Close();
            
            XDocument doc = new XDocument();
            XElement root = new XElement("Root");
            doc.Add(root);
            doc.Save(path);
        }
    }
}
