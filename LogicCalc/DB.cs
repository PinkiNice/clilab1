﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LogicCalc
{
    public class DB
    {
        private List<Entry> db = new List<Entry>();

        public Boolean Add(Entry entry)
        {
            db.Add(entry);
            return true;
        }
        
        public List<Entry> getAll()
        {
            return db;
        }

        public string GetAsString(int i)
        {
            if (0 <= i && i < db.Count())
            {
                return db.ElementAt(i).ToString();
            }
            return "No Such Element!";
        }

        public Entry GetLast()
        {
            Entry[] arr = db.ToArray();
            return arr[arr.Length];
        }

    }
}
