﻿using System;

public class MenuCommand
{
    public delegate void EventHandler();
    private string _message;

    private EventHandler _handler = delegate
    {
        Console.WriteLine("No Action");
    };

    public EventHandler Handler
    {
        set
        {
            _handler = value;
        }
    }

    public void CallHandler()
    {
        _handler.Invoke();
    }

    public MenuCommand(string msg, EventHandler handler)
    {
        _message = msg;
        _handler = handler;
    }

    public MenuCommand(string msg)
    {
        _message = msg;
    }

    public string Message
    {
        get
        {
            return _message;
        }

        set
        {
            _message = value;
        }
    }
            
    public bool Fit(string userCmd)
    {
        int stringCompare = String.Compare(_message.ToLower(), userCmd.ToLower());

        if (!Convert.ToBoolean(stringCompare))
        {
            return true;
        }
        return false;
    }

}
