﻿using System;

public class UserRequest
{
    private string _input;

    public string Input
    {
        get
        {
            return _input;
        }
        set
        {
            _input = value.ToLower();
        }
    }

    public bool isExit()
    {
        return (_input == "exit");
    }

    public void Get()
    {
        Console.WriteLine("Your Command:");
        Input = Console.ReadLine();
    }
}
