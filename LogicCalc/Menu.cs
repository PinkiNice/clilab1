﻿using System;
using System.Collections.Generic;
public class Menu
{
    private List<MenuCommand> commands = new List<MenuCommand>();

	public Menu()
	{
	}

    public void AddCommand(MenuCommand command)
    {
        commands.Add(command);
    }

    public void Show()
    {
        foreach (MenuCommand cmd in commands)
        {
            Console.WriteLine(cmd.Message);
        }
    }

    public void Call(string req)
    {
        foreach (MenuCommand cmd in commands)
        {
            if (cmd.Fit(req))
            {
                cmd.CallHandler();
                return;
            }
        }
        Console.WriteLine("No Such Command");
    }
}
