﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicCalc
{
    class Calc
    {
        private static bool xor(bool x, bool y)
        {
            return x ^ y;
        }

        private static bool or(bool x, bool y)
        {
            return x || y;
        }

        private static bool and(bool x, bool y)
        {
            return x && y;
        }

        private static bool not(bool x, bool y = false)
        {
            return !x;
        }
        public delegate Boolean OperandDelegate(Boolean x, Boolean y);
       
        private static Dictionary<OperationKind, OperandDelegate> operatorsDict = new Dictionary<OperationKind, OperandDelegate>
        {
            { OperationKind.Xor, new OperandDelegate(xor) },
            { OperationKind.Or,  new OperandDelegate( or) },
            { OperationKind.And, new OperandDelegate(and) },
            { OperationKind.Not, new OperandDelegate(not) }
        };

        public static bool OperatorExists(OperationKind operation)
        {
            return operatorsDict.ContainsKey(operation);
        }

        public static bool Count(OperationKind operation, bool x, bool y = false)
        {
            OperandDelegate result;
            
            if (!operatorsDict.TryGetValue(operation, out result))
               throw new Exception("There is no '{0}' operator");

            return result(x, y);
        }
    }
}