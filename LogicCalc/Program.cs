﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;
namespace LogicCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            string dbPath = null;

            Console.WriteLine("Hello!");
            while (dbPath == null)
            {
                Console.WriteLine("Please, enter a data base file name");
                dbPath = GetDbPath();
                if (!File.Exists(dbPath))
                {
                    try
                    {
                        XmlDb.MakeDbFile(dbPath);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("This is not valid file name. Please, try again.");
                        dbPath = null;
                    }
                }
            }

            Console.WriteLine("Initializing...");

            DB db = new DB();
            Menu menu = new Menu();
            UserRequest request = new UserRequest();

            MenuCommand cmdDo = new MenuCommand("do", delegate { Do(db); });
            MenuCommand cmdExit = new MenuCommand("exit", delegate { dbSaveXml(db, dbPath); });
            MenuCommand cmdList = new MenuCommand("list", delegate { PrintAll(db); });
            MenuCommand cmdGet = new MenuCommand("get", delegate { Get(db); });
            MenuCommand cmdSave = new MenuCommand("save", delegate { dbSaveXml(db, dbPath); });

            menu.AddCommand(cmdDo);
            menu.AddCommand(cmdGet);
            menu.AddCommand(cmdList);
            menu.AddCommand(cmdSave);
            menu.AddCommand(cmdExit);

            dbLoadXml(db, dbPath);
            
            while (!request.isExit())
            {
                menu.Show();
                request.Get();
                menu.Call(request.Input);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine("Bye!..");
            
        }

        public static Boolean UserInputParse(string valueName, out Boolean result)
        {
            Console.WriteLine(valueName);
            if (!Boolean.TryParse(Console.ReadLine().Trim(' '), out result))
            {
                Console.WriteLine("Wrong value");
                return false;
            }
            return true;
        }

        class OpRequestInfo
        {
            public bool A { get; private set; }
            public bool B { get; private set; }
            public OperationKind Op { get; private set; }

            public OpRequestInfo(bool a, bool b, OperationKind op)
            {
                A = a;
                B = b;
                Op = op;
            }
        }

        static bool GetOpRequest(out OpRequestInfo req)
        {
            string input = Console.ReadLine().Trim();
            string formattedInput = Char.ToUpper(input[0]) + input.Substring(1).ToLower();
            OperationKind op;
            bool x = false, y = false;
            req = null;

            if (Enum.TryParse<OperationKind>(formattedInput, out op))
            {
                if (UserInputParse("x", out x))
                {
                    if (!(op == OperationKind.Not))
                    {
                        if (!UserInputParse("y", out y))
                        { 
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

            req = new OpRequestInfo(x, y, op);
            return true;
        }

        static void PerformOp(DB db, OpRequestInfo req)
        {
            Boolean result = Calc.Count(req.Op, req.A, req.B);
            Entry entry = new Entry(req.A, req.Op, req.B, result);
            Console.WriteLine(entry.ToString());
            db.Add(entry);
        }

        public static void Do(DB dataBase)
        {

            OpRequestInfo request;
            bool success = GetOpRequest(out request);

            if (success)
            {
                PerformOp(dataBase, request);
            }
            else
            {
                Console.WriteLine("Fail");
                return;
            }
        }

        public static void Get(DB dataBase)
        {
            int i = -1;
            Console.WriteLine("Number of operation =");
            if (!int.TryParse(Console.ReadLine().Trim(' '), out i))
            {
                Console.WriteLine("Wrong value");
                return;
            }
           
            Console.WriteLine(dataBase.GetAsString(i - 1));
            return;
        }

        public static void PrintAll(DB dataBase)
        {
            Console.WriteLine("----------------------");
            List<Entry> arr = dataBase.getAll();
            foreach (Entry entry in arr)
            {
                Console.WriteLine(entry.ToString());
            }
        }

        public static void dbLoadXml(DB _db, string _xmlDbPath)
        {
            XmlDb xmlDb = new XmlDb(_xmlDbPath);
            XDocument list = xmlDb.getAll();
            foreach (XElement xmlEntry in list.Element("Root").Nodes())
            {
                _db.Add(toEntry(xmlEntry));
            }
        }

        static public XElement toXml(Entry entry)
        {
            XElement element = new XElement("Entry",
                new XElement("Operation", entry.Operation),
                new XElement("X", entry.X),
                new XElement("Y", entry.Y),
                new XElement("Result", entry.Result));

            return element;
        }

        public static void dbSaveXml(DB _db, string _xmlDbPath)
        {
            XDocument doc = new XDocument();

            XElement root = new XElement("Root");

            List<Entry> list = _db.getAll();
            foreach (Entry entry in list) 
            {
                root.Add(toXml(entry));
            }

            doc.Add(root);
            doc.Save(_xmlDbPath);
        }

        public static Entry toEntry(XElement xmlEntry)
        {
            Boolean y = false;

            OperationKind op = (OperationKind)Enum.Parse(typeof(OperationKind), xmlEntry.Element("Operation").Value);
            if (op != OperationKind.Not)
            {
                y = Convert.ToBoolean(xmlEntry.Element("Y").Value);
            }

            Boolean x = Convert.ToBoolean(xmlEntry.Element("X").Value);
            Boolean result = Convert.ToBoolean(xmlEntry.Element("Result").Value);
            return new Entry(x, op, y, result);
        }

        public static string GetDbPath()
        {
            string path = Console.ReadLine();
            return path;
        }
    }
}