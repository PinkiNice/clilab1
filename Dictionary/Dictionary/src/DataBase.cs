﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace Dictionary.src
{
    class DataBase
    {
        Dictionary<string, List<Entry>> entriesDict = new Dictionary<string, List<Entry>>();
        string filePath = null;

        //load Database from existing File or create a new one
        public DataBase(string filePath)
        {
            this.filePath = filePath;

            if (File.Exists(filePath))
            {
                try
                {
                    //Contains Valid XML so everything is ok, we've loaded our dict
                    DataContractSerializer dataContractSerializer = new DataContractSerializer(typeof(Dictionary<string, List<Entry>>));
                    using (FileStream path = File.OpenRead(filePath))
                    {
                        XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(path, new XmlDictionaryReaderQuotas());
                        entriesDict = (Dictionary<string, List<Entry>>)dataContractSerializer.ReadObject(reader);
                    };
                }
                catch (Exception e)
                {
                    //File exists but contains shit
                    Console.WriteLine(e.ToString());
                    using (FileStream stream = File.Create(filePath)) { };
                }
            }
            else
            {
                //File doesn't exists, so we just create a new one
                using (FileStream stream = File.Create(filePath)) { };
            }
        }

        //save DataBase to existing or new file
        public void Save(string filePath)
        {
            DataContractSerializer writer = new DataContractSerializer(typeof(Dictionary<string, List<Entry>>));
            FileStream path = File.Create(filePath);

            writer.WriteObject(path, entriesDict);
        }


        //save DataBase to the file, where it was loaded from
        public void Save()
        {
            Save(filePath);
        }

        //Add new word to our dataBase
        public void Add(Entry entry)
        {
            string root = entry.root;
            if (!IsRootExists(root))
            {
                entriesDict[root] = new List<Entry>();
            }
            entriesDict[root].Add(entry);
        }

        //Checks whether our dataBase already have some root in itself
        public Boolean IsRootExists(string root)
        {
            if (entriesDict.ContainsKey(root))
            {
                return true;
            }
            return false;
        }

        /*********************************************************
        Checks whether we have some particular word in our DataBase
        Returns: Root under which the word were found
        **********************************************************/
        public string RootIfKnown(string word)
        {
            List<string> potentialRoots = PotentialRoots(word);
            foreach (string root in potentialRoots)
            {
                if (KnownWordsWithRoot(root).Contains(word))
                {
                    return root;
                }
            }
            return null;
        }

        /*************************************************************************************
        Returns: List<string> that contains all known words with particular root, passed as arg.

        Exceptions: If passed root aren't known to our DB it will raise an Exception.
        Example: For passed root "ever" it will return [forever, never, ever, clever, ..., etc]
        TODO: problems with the same roots: Ever - Clever
        ***************************************************************************************/
        public List<string> KnownWordsWithRoot(string root)
        {
            List<string> knownWordsWithRoot = new List<string>();

            if (!entriesDict.ContainsKey(root))
            {
                throw new Exception("Exception in the method 'KnownWordsWithRoot' - root in args aren't known to DB");
            }

            foreach (Entry entry in entriesDict[root])
            {
                knownWordsWithRoot.Add(entry.ToString());
            }
            return knownWordsWithRoot;
        }

        /*********************************************************************************
        Returns a List<string> that contains all known roots that may fit.

        Example: for word "nevergreen" it may have [green, neverg, vergreen, een, ..., etc]
        If all of those are known to our DataBase 
        **********************************************************************************/
        private List<string> PotentialRoots(string word)
        {
            List<string> potentialRoots = new List<string>();
            List<string> allRoots = new List<string>(entriesDict.Keys);
            
            foreach (string root in allRoots)
            {
                if (word.IndexOf(root) != -1)
                {
                    potentialRoots.Add(root);
                }
            }
            return potentialRoots;
        }
    }
}
