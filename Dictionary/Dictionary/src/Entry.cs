﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Dictionary.src
{
    [DataContract(Name = "EntryContract")]
    public class Entry
    {
        [DataMember(Name = "WordPrefix")]
        public List<string> prefix;
        [DataMember(Name = "WordRoot")]
        public string root;
        [DataMember(Name = "WordSuffix")]
        public List<string> suffix;
            
        public string Root
        {
            get { return root; }
        }
        //For serialization
        public Entry() {}

        public Entry(List<string> prefix, string root, List<string> suffix)
        {
            this.prefix = prefix;
            this.root = root;
            this.suffix = suffix;
        }

        public override string ToString()
        {
            string word = "";
            foreach (string prefix in this.prefix)
            {
                word += prefix;
            }
            word += root;
            foreach (string suffix in this.suffix)
            {
                word += suffix;
            }

            return word;
        }
    }
}
