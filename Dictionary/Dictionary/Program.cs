﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dictionary.src;

namespace Dictionary
{
    class Program
    {

        const string filePath = "./file.xml";

        static void PrintList(List<string> list)
        {
            foreach (string word in list)
            {
                Console.WriteLine(word);
            }
        }

        static void Main(string[] args)
        {
            Boolean exit = false;
            DataBase db = new DataBase(filePath);
            string word;
            string wordRoot;
            Entry wordByParts;

            while (!exit)
            {
                word = UserInterface.GetWord();
                if (word == "q")
                {
                    //exit
                    exit = true;
                    db.Save();
                    continue;
                } else if (word.Length == 0)
                {
                    continue;
                }
           
                wordRoot = db.RootIfKnown(word);

                //word is known
                if (wordRoot != null)
                {
                    //showResult
                    PrintList(db.KnownWordsWithRoot(wordRoot));
                }

                //word is unknown
                else
                {
                    //user wants to add it
                    if (UserInterface.YesNot("It's unknown word. Do you want to add it?"))
                    {
                        //form by parts
                        wordByParts = UserInterface.GetWordByParts();

                        //word is the same as was?
                        if (String.CompareOrdinal(wordByParts.ToString(), word) == 0)
                        {
                            //add to the data base
                            db.Add(wordByParts);
                            Console.WriteLine("Your word now is in db!" + '\n');
                        }
                    }
                    else
                    {
                        Console.WriteLine('\n');
                    }
                }
            }
        }
    }
}
