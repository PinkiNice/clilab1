﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewDict.Model
{
    public interface IDictionaryDb
    {
        void Add(Entry entry);
        List<string> Results(string word);
        Boolean IsRootExists(string root);
        string RootIfKnown(string word);
        List<string> KnownWordsWithRoot(string root);
        //save DataBase to existing or new file
        void SaveTo(string filePath);
        //save DataBase to the file, where it was loaded from
        void Save();
        
    }
}
