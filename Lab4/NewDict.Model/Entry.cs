﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewDict.Model
{
    public class Entry
    {
        public ReadOnlyCollection<string> Prefix { get; private set; }
        public string Root { get; private set; }
        public ReadOnlyCollection<string> Suffix { get; private set; }

        public Entry(List<string> prefix, string root, List<string> suffix)
        {
            this.Prefix = new ReadOnlyCollection<string>(prefix.ToArray());
            this.Root = root;
            this.Suffix = new ReadOnlyCollection<string>(suffix.ToArray());
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string prefix in this.Prefix)
                sb.Append(prefix);

            sb.Append(this.Root);
            foreach (string suffix in this.Suffix)
                sb.Append(suffix);

            return sb.ToString();
        }

        public string PrettyString()
        {
            string word = "";
            if (Prefix != null)
            {
                string prefix = string.Join("-", this.Prefix);
                word += prefix + "-";
            }
            word += Root;
            if (Suffix != null)
            {
                string suffix = string.Join("-", this.Suffix);
                word += "-" + suffix;
            }

            return word;
        }
    }
}
