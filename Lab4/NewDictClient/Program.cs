﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewDict.Model;
using NewDict.Interaction.Impl;
using System.Xml;

namespace NewDictClient
{
    class Program
    {
        const string end = "net.pipe://localhost/NorthwindSvc/CustomerService";

        static void Main(string[] args)
        {
            InsertEndpoint("gg");
            Console.ReadKey();
            using (var client = new DictionaryClient())
            {
                
                Boolean exit = false;

                string word = null;
                string wordRoot = null;
                Entry wordByParts = null;

                while (!exit)
                {
                    word = UserInterface.GetWord();
                    if (word == "q")
                    {
                        exit = true;
                        client.Save();
                        continue;
                    }

                    wordRoot = client.RootIfKnown(word);

                    if (wordRoot != null)
                    {
                        PrintList(client.KnownWordsWithRoot(wordRoot));
                    }

                    else
                    {
                        if (UserInterface.YesNot("It's unknown word. Do you want to add it?"))
                        {
                            wordByParts = UserInterface.GetWordByParts();
                            Console.WriteLine("Your Word: {0}", wordByParts.ToString());
                            if (String.CompareOrdinal(wordByParts.ToString(), word) == 0)
                            {
                                client.Add(wordByParts);
                            }
                            else
                            {
                                Console.WriteLine("It's a different word!~~~");
                            }
                        }
                    }
                }
            }
        }
        static void PrintList(List<string> list)
        {
            foreach (string word in list)
            {
                Console.WriteLine(word);
            }
        }

        static void InsertEndpoint(string endpoint)
        {
            string appConfigFile = "../../App.config";
            XmlDocument doc = new XmlDocument();
            doc.Load(appConfigFile);
            XmlNodeList endpointNode = doc.SelectNodes("/configuration/system.serviceModel/client/endpoint");
            endpointNode[0].Attributes["address"].InnerText = endpoint;
            doc.Save("../../App.config");
        }
    }
}
