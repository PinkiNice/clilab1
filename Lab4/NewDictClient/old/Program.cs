﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using DictShared;
//using DictClientOld.src;
//using System.ServiceModel;

//namespace DictClientOld
//{
//    class Program
//    {
//        const string filePath = "./myLittleDb.xml";

//        static void OldMain(string[] args)
//        {
//            Boolean exit = false;
//            RemoteDataBaseControl db = new RemoteDataBaseControl(filePath);

//            string word = null;
//            string wordRoot = null;
//            Entry wordByParts = null;

//            while (!exit)
//            {
//                word = UserInterface.GetWord();
//                if (word == "q")
//                {
//                    exit = true;
//                    db.Save();
//                    continue;
//                }

//                wordRoot = db.RootIfKnown(word);

//                if (wordRoot != null)
//                {
//                    PrintList(db.KnownWordsWithRoot(wordRoot));
//                }

//                else
//                {
//                    if (UserInterface.YesNot("It's unknown word. Do you want to add it?"))
//                    {
//                        wordByParts = UserInterface.GetWordByParts();
//                        Console.WriteLine("Your Word: {0}", wordByParts.ToString());
//                        if (String.CompareOrdinal(wordByParts.ToString(), word) == 0)
//                        {
//                            db.Add(wordByParts);
//                        }
//                        else
//                        {
//                            Console.WriteLine("It's a different word!~~~");
//                        }
//                    }
//                }
//            }
//        }

//        static void PrintList(List<string> list)
//        {
//            foreach (string word in list)
//            {
//                Console.WriteLine(word);
//            }
//        }
//    }
//}
