﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace NewDictServer
{
    class Program
    {
        public static DictionaryDb DbInstance { get; private set; }

        static void Main(string[] args)
        {
            DbInstance = new DictionaryDb("myDataBase.xml");

            using (ServiceHost host = new ServiceHost(typeof(DictionaryServiceImpl)))
            {
                host.Open();

                Console.WriteLine("Service up and running at:");
                foreach (var ea in host.Description.Endpoints)
                {
                    Console.WriteLine(ea.Address);
                }

                Console.ReadLine();
                host.Close();
            }
        }
    }
}
