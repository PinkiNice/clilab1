﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Web;
using System.IO;
using NewDict.Interaction;
using NewDict.Model;

namespace NewDictServer
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.Single)]
    class DictionaryServiceImpl : IDictionaryService
    {
        DictionaryDb _db;
        string fileName;

        public DictionaryServiceImpl()
        {
            _db = Program.DbInstance;
            fileName = _db.filePath;
        }

        public void SaveTo(string name)
        {
            if (name.Any(c => Path.GetInvalidFileNameChars().Contains(c)))
                throw new ArgumentException();

            var dbDirPath = Path.Combine(Environment.CurrentDirectory, "DictionaryServiceDbFiles");
            if (!Directory.Exists(dbDirPath))
                Directory.CreateDirectory(dbDirPath);

            var dbFilePath = Path.Combine(dbDirPath, name + ".xml");
            _db.SaveTo(dbFilePath);
        }

        public void Save()
        {
            SaveTo(this.fileName);
        }

        public void Add(DictEntryInfo entry)
        {
            _db.Add(new Entry(entry.Prefix, entry.Root, entry.Suffix));
        }

        public bool IsRootExists(string root)
        {
            return _db.IsRootExists(root);
        }

        public List<string> KnownWordsWithRoot(string root)
        {
            return _db.KnownWordsWithRoot(root);
        }

        public List<string> Results(string word)
        {
            return _db.Results(word);
        }

        public string RootIfKnown(string word)
        {
            return _db.RootIfKnown(word);
        }
    }
}
