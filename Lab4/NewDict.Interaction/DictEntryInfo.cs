﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

namespace NewDict.Interaction
{
    [DataContract]
    public class DictEntryInfo
    {
        [DataMember]
        public List<string> Prefix { get; set; }
        [DataMember]
        public string Root { get; set; }
        [DataMember]
        public List<string> Suffix { get; set; }

        public DictEntryInfo()
        {

        }
    }
}
