﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using NewDict.Model;

namespace NewDict.Interaction.Impl
{
    public class DictionaryClient : IDictionaryDb, IDisposable
    {
        class DictServiceClientImpl : ClientBase<IDictionaryService>
        {
            public IDictionaryService Proxy { get { return this.Channel; } }
        }

        readonly DictServiceClientImpl _client;
        readonly IDictionaryService _svc;

        public DictionaryClient()
        {
           
            _client = new DictServiceClientImpl();
            _client.Open();
            _svc = _client.Proxy;
        }

        public void Add(Entry entry)
        {
            _svc.Add(new DictEntryInfo()
            {
                Prefix = entry.Prefix.ToList(),
                Root = entry.Root,
                Suffix = entry.Suffix.ToList()
            });
        }

        public bool IsRootExists(string root)
        {
            return _svc.IsRootExists(root);
        }

        public List<string> KnownWordsWithRoot(string root)
        {
            return _svc.KnownWordsWithRoot(root);
        }

        public List<string> Results(string word)
        {
            return _svc.Results(word);
        }

        public string RootIfKnown(string word)
        {
            return _svc.RootIfKnown(word);
        }

        public void SaveTo(string fileName)
        {
            _svc.SaveTo(fileName);
        }

        public void Save()
        {
            _svc.Save();
        }
        public void Dispose()
        {
            _client.Close();
        }
    }
}
