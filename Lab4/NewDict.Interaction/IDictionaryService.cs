﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Web;

namespace NewDict.Interaction
{
    [ServiceContract]
    public interface IDictionaryService
    {
        [OperationContract]
        void Add(DictEntryInfo entry);

        [OperationContract]
        List<string> Results(string word);

        [OperationContract]
        bool IsRootExists(string root);

        [OperationContract]
        List<string> KnownWordsWithRoot(string root);

        [OperationContract]
        string RootIfKnown(string word);

        [OperationContract]
        void SaveTo(string fileName);

        [OperationContract]
        void Save();
    }
}
