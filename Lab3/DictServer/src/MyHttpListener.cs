﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
namespace DictServer.src
{
    class MyHttpListener
    {
        Socket mainSocket;
        IPAddress localIp;
        IPEndPoint localEndPoint;
        Boolean isWorking = false;
        public string[] Prefixes { get; }

        public MyHttpListener(string ip, int port)
        {
            localIp = IPAddress.Parse(ip);
            localEndPoint = new IPEndPoint(localIp, 8080);
            mainSocket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
        }

        public MyHttpListenerContext GetContext()
        {
            if (!isWorking)
            {
                throw new Exception("HttpListener is not working");
            }
            string data = null;
            byte[] bytes = new Byte[1024];

            Socket handler = mainSocket.Accept();

            bytes = new byte[1024];
            int bytesReceived = handler.Receive(bytes);
            data += Encoding.ASCII.GetString(bytes, 0, bytesReceived);
            
            return new MyHttpListenerContext(data, handler);
        }

        public void Start()
        {
            mainSocket.Bind(localEndPoint);
            mainSocket.Listen(10);
            
            isWorking = true;
        }

    }
}
