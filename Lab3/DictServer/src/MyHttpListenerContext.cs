﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace DictServer.src
{
    class MyHttpListenerContext
    {
        MyHttpListenerRequest request;
        MyHttpListenerResponse response;
        Socket handler;

        public Socket Handler
        {
            get { return handler; }
        }
        public MyHttpListenerContext(string request, Socket handler)
        {
            this.handler = handler;
            response = new MyHttpListenerResponse(new NetworkStream(handler));
            this.request = new MyHttpListenerRequest(request);
        }
        public MyHttpListenerRequest Request
        {
            get { return request; }
        }
        public MyHttpListenerResponse Response
        {
            get { return response; }
        }

      
    }
}
