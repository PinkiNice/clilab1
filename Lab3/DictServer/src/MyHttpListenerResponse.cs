﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace DictServer.src
{
    class MyHttpListenerResponse
    {
        public int StatusCode { get; set; }
        public Boolean KeepAlive { get; set; }
        public long ContentLength64 { get; set; }
        private Stream outputStream = null;

        public MyHttpListenerResponse(Stream outputStream)
        {
            this.outputStream = outputStream;
        }
        public Stream OutputStream()
        {
            WriteHttpHeader(StatusCode);

            WriteHeader("Server", "MOYSERVAK");
            WriteHeader("Date", DateTime.Now.ToString());
            WriteHeader("Content-Length", ContentLength64.ToString());
            WriteHeader("Connection", (KeepAlive ? "keep-alive" : "close"));
            EndHeaders();
            return outputStream;
        }
        private void WriteHttpHeader(int StatusCode)
        {
            string httpHeader = "HTTP/1.1 " + StatusCode.ToString() + " OK(I HOPE)\r\n";
            WriteString(httpHeader);
        }
        private void WriteHeader(string headerName, string header)
        {
            WriteString(headerName + ":");
            WriteString(header + "\r\n");
        }
        private void WriteString(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            outputStream.Write(bytes, 0, bytes.Length);
        }
        private void EndHeaders()
        {
            WriteString("\r\n\r\n");
        }
    }
}