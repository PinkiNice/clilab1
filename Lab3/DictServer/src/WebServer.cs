﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Net.Http;
using DictServer.src;
/**************************************************************************************
This class is responsible for catching connections and providing context to outer space
***************************************************************************************/
namespace DictServer
{
    class WebServer
    {
        /***************************************************************
        string - prefix
        Func is the handler function for that prefix
        ****************************************************************/
        Dictionary<string, Func<MyHttpListenerContext, string>> handlers;
        MyHttpListener listener;
        string prefix;

        //Adjustment
        string hostName;
        int hostPort;

        public WebServer(string hostName, int hostPort)
        {
            this.hostName = hostName;
            this.hostPort = hostPort;
            handlers = new Dictionary<string, Func<MyHttpListenerContext, string>>();
            listener = new MyHttpListener(hostName, hostPort);
        }
        /*
        public WebServer(string prefix)
        {
            handlers = new Dictionary<string, Func<MyHttpListenerContext, string>>();
            listener = new MyHttpListener(hostName, hostPort);
            //listener.Prefixes.Add(prefix);
            this.prefix = prefix;
        }
        */

        /***********************************************
        Handle received requests, chooses handle
        ************************************************/
        public void ProcessRequest(MyHttpListenerContext context)
        {
            string response = "";
            MyHttpListenerRequest request = context.Request;
            string url = request.Url;
            string path = url.Substring(0, url.IndexOf("?") == -1 ? url.Length : url.IndexOf("?"));
            Console.WriteLine("Requested Path: {0}", path);
            Console.WriteLine("Query String: {0}", url);

            foreach (string key in handlers.Keys)
            {
                Console.WriteLine(key);
            }
            if (handlers.ContainsKey(path))
            {
                response = handlers[path](context);
            }
            else
            {
                context.Response.StatusCode = 404;
                response = "404";
            }
            Response(response, context);
        }

        /*********************
        Run server
        **********************/
        public void Start()
        {
            listener.Start();
            while (true)
            {
                try
                {
                    MyHttpListenerContext context = listener.GetContext();
                    try
                    {
                        ProcessRequest(context);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Failed to process request");
                        Console.WriteLine(e.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to get context");
                    Console.WriteLine(e.ToString());
                }
            }
        }   

        /*******************************************************
        *Encodes data and sends
        ********************************************************/
        public void Response(string response, MyHttpListenerContext context)
        {
            byte[] byteResponse = Encoding.ASCII.GetBytes(response);
            //Status code is set in handler functions
            context.Response.KeepAlive = false;
            context.Response.ContentLength64 = byteResponse.Length;
            
            Stream output = context.Response.OutputStream();
            output.Write(byteResponse, 0, byteResponse.Length);
            context.Handler.Shutdown(SocketShutdown.Both);
            context.Handler.Close();
        }

        /***********************************************************
        *Adds Handler and Prefix, which will be handled by handler.
        ***********************************************************/
        public void AddHandler(string prefix, Func<MyHttpListenerContext, string> handlerFunction)
        {
            handlers.Add(prefix, handlerFunction);
        }
    }
}

