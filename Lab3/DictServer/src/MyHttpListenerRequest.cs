﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
namespace DictServer.src
{
    class MyHttpListenerRequest
    {
        private string query;
        private string url;

        public MyHttpListenerRequest(string data)
        {
            url = EjectUrl(data);
            query = EjectQuery(url);
        }
        public string Query
        {
            get { return query; }
        }

        public string Url
        {
            get { return url; }
        }

        static private string EjectUrl(string data)
        {
            try {
                int start = data.IndexOf(' ') + 1;
                int end = data.IndexOf(' ', start + 1);
                string url = data.Substring(start, end - start);
                return url;
            }
            catch
            {
                return "";
            }
        }
        static private string EjectQuery(string url)
        {
            int signPos = url.IndexOf("?");
            if (signPos != -1)
            {
                return url.Substring(signPos);
            }
            return "";
        }
    }
}
