﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net.Http;
using System.Net;
using Dictionary.src;
using System.Web;
using DictShared;

namespace DictServer
{
    class Program
    {
        const string localHost = "http://127.0.0.1:8080/";
        const int port = 8080;
        const string filePath = "./file.xml";
        static IDataBase db;

        static void Main1(string[] args)
        {
            db = new DataBase(filePath);

            WebServer server = new WebServer(localHost);
            server.AddHandler("/add", Add);
            server.AddHandler("/result", Result);
            server.Start();
        }

        /****************************************************
        *Parses Url Query for requests, on /Add url
        *****************************************************/
        static Entry ParseAddQuery(string query)
        {
            string rawPrefixes = HttpUtility.ParseQueryString(query).Get("prefixes");
            string root = HttpUtility.ParseQueryString(query).Get("root");
            string rawSuffixes = HttpUtility.ParseQueryString(query).Get("suffixes");

            List<string> prefixes = Separate(rawPrefixes, ',');
            List<string> suffixes = Separate(rawSuffixes, ',');

            if (root == null)
            {
                return null;
            }
            
            Entry entry = new Entry(prefixes, root, suffixes);
            return entry;
        }

        /***********************************************************************************
        *<Summary> Splits string by choosen separator
        *<Difference> returns Null if splitting is impossible
        ************************************************************************************/
        static List<string> Separate(string str, char sep)
        {
            List<string> separated;

            if (str != null)
            {
                separated = new List<string>(str.Split(sep));
            }
            else
            {
                separated = new List<string>();
            }

            return separated;
        }

        /**********************************************
        *Handler function for ./results
        ***********************************************/
        static string Result(HttpListenerContext context)
        {
            string response = "<html><body>unknown<body><html>";

            //result?word=word&
            HttpListenerRequest request = context.Request;
            Uri url = request.Url;
            string query = url.Query;
            string word = HttpUtility.ParseQueryString(query).Get("word");

            if (word == null)
            {
                return response;
            }

            Console.WriteLine("Requested Word:{0}", word);
            string wordRoot = db.RootIfKnown(word);
            Console.WriteLine("Root of this word is:{0}", wordRoot);

            //word is known
            if (wordRoot != null)
            {
                
                //return List<string>
                //showResult
                response = FormResponse(db.KnownWordsWithRoot(wordRoot));
            }
            
            return response;
        }

        /**********************************************
        *Handler function for ./Add
        ***********************************************/
        static string Add(HttpListenerContext context)
        {
            //add?prefixes=p1,p2,p3&root=root&suffixes=s1,s2,s3&
            string response = "fail";

            HttpListenerRequest request = context.Request;
            Uri url = request.Url;
            string query = url.Query;

            Entry entry = ParseAddQuery(query);
            //If entry is parsed succesfully from query string.
            if (entry != null)
            {
                db.Add(entry);
                db.Save();
                response = String.Format("Added succesfully your word: {0}", entry.PrettyString());
            }
            //If we were unable to parse query string properly
            else
            {
                response = "400";
            }

            return response;
        }

        /**************************************************
        *This is probably unused in this project.
        *Just puts string into standart html construction
        **************************************************/
        static string FormResponse(List<string> list)
        {
            string response = "";
            foreach (string word in list)
            {
                response += "\n" + word;
            }
            return response;
        }

    }
}
