﻿using System.Collections.Generic;
using DictShared;

namespace DictShared
{
    interface IDataBase
    {
        void Add(Entry entry);
        List<string> Results(string word);
        bool IsRootExists(string root);
        List<string> KnownWordsWithRoot(string root);
        string RootIfKnown(string word);
        void Save();
    }
}