﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DictShared
{
    [DataContract(Name = "EntryContract")]
    public class Entry
    {
        [DataMember(Name = "WordPrefix")]
        public List<string> prefix;

        [DataMember(Name = "WordRoot")]
        public string root;

        [DataMember(Name = "WordSuffix")]
        public List<string> suffix;

        public string Root
        {
            get { return root; }
        }
        //For serialization
        public Entry() { }

        public Entry(List<string> prefix, string root, List<string> suffix)
        {
            this.prefix = prefix;
            this.root = root;
            this.suffix = suffix;
        }

        public override string ToString()
        {
            string word = "";
            foreach (string prefix in this.prefix)
            {
                word += prefix;
            }
            word += root;
            foreach (string suffix in this.suffix)
            {
                word += suffix;
            }

            return word;
        }

        public string PrettyString()
        {
            string word = "";
            if (prefix != null)
            {
                string prefix = string.Join("-", this.prefix);
                word += prefix + "-";
            }
            word += root;
            if (suffix != null)
            {
                string suffix = string.Join("-", this.suffix);
                word += "-" + suffix;

            }
            return word;
        }
    }
}