﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DictShared;
using DictClient.src;

namespace DictClient
{
    class Program
    {
        const string filePath = "./file.xml";

        static void PrintList(List<string> list)
        {
            foreach (string word in list)
            {
                Console.WriteLine(word);
            }
        }

        static void Main(string[] args)
        {
            Boolean exit = false;
            DataBaseClient db = new DataBaseClient("127.0.0.1", 8080);
            List<string> sameRootWords;
            Entry wordByParts;

            string word;
            while (!exit)
            {
                word = UserInterface.GetWord();
                if (word == "q")
                {
                    exit = true;
                    continue;
                }
                sameRootWords = db.Results(word);

                //word is known
                if (sameRootWords != null)
                {
                    PrintList(sameRootWords);
                }

                //word is unknown
                else
                {
                    if (UserInterface.YesNot("It's unknown word. Do you want to add it?"))
                    {
                        wordByParts = UserInterface.GetWordByParts();
                        Console.WriteLine("Your Word: {0}", wordByParts.ToString());
                        //word is the same as was?
                        if (String.CompareOrdinal(wordByParts.ToString(), word) == 0)
                        {
                            //add to the data base and print out response
                            Console.WriteLine(db.Add(wordByParts));
                        }
                        else
                        {
                            Console.WriteLine("It's a different word!~~~");
                        }
                    }
                }
            }
        }
    }
}
