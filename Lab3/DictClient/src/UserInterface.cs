﻿using System;
using System.Collections.Generic;
using DictShared;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictClient.src
{
    static class UserInterface
    {
        static public string Get(string item)
        {
            Console.WriteLine("Please Enter The {0}", item);
            string result = Console.ReadLine();
            return result;
        }

        static public List<string> GetPart(string wordPart)
        {
            List<string> wholePart = new List<string>();

            string current = Get(wordPart);

            while (current != "")
            {
                wholePart.Add(current);
                current = Get(wordPart);
            }
            return wholePart;
        }

        static public string Prefix()
        {
            return Get("Prefix");
        }

        static public string GetRoot()
        {
            return Get("Root");
        }

        static public string Suffix()
        {
            return Get("Suffix");
        }

        static public string GetWord()
        {
            return Get("Word");
        }

        static public Entry GetWordByParts()
        {
            List<string> prefix = GetPart("Prefix");
            string root = GetRoot();
            List<string> suffix = GetPart("Suffix");

            Entry formedWord = new Entry(prefix, root, suffix);

            return formedWord;
        }

        static public Boolean YesNot(string message)
        {
            Console.WriteLine(message + ": y|n");
            ConsoleKey key = Console.ReadKey().Key;

            while (true)
            {
                if (key == ConsoleKey.Y) return true;
                else if (key == ConsoleKey.N) return false;
            }
        }
    }
}