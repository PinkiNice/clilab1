﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Net.Sockets;
using DictShared;

namespace DictClient.src
{
    class DataBaseClient
    {
        private string hostName;
        private ushort port;
        private IPEndPoint hostEp;
        private IPAddress hostIp;
        private Socket sender;

        public DataBaseClient(string hostName, ushort port)
        {
            this.hostName = hostName;
            this.port = port;
            this.hostIp = IPAddress.Parse(hostName);
            this.hostEp = new IPEndPoint(hostIp, port);
            this.sender = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
        }

        //Request Functions
        public string Add(Entry entry)
        {
            string requestUrl = "/add" + EntryToQuery(entry);
            string response = SendReceive(requestUrl);
            return extractData(response);
        }
        public List<string> Results(string word)
        {
            string requestUrl = "/result?word=" + word;
            string response = SendReceive(requestUrl);
            string data = extractData(response);
            if (data.IndexOf("unknown word") == -1)
            {
                return new List<string>(data.Split('\n'));
            }
            return null;
        }

        //Sockets
        private bool Connect()
        {
            try
            {
                sender.Connect(hostEp);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }
        private string SendReceive(string url)
        {
            sender.Connect(hostEp);
            sender.Send(Encoding.ASCII.GetBytes(formHttpRequest(url)));
            string response = ReceiveAllData();
            PrepareSocket();
            return response;
        }
        private string ReceiveAllData()
        {
            byte[] bytes = new byte[1024];
            int size = sender.Receive(bytes);
            string response = null;
            while (size != 0)
            {
                response += Encoding.ASCII.GetString(bytes, 0, size);
                size = sender.Receive(bytes);
            }
            return response;
        }
        private void PrepareSocket()
        {
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
            sender.Dispose();
            sender = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
        }

        //Html helpers
        private string extractData(string htmlResponse)
        {
            int index = htmlResponse.IndexOf("\r\n\r\n");
            if (index != -1)
            {
                return htmlResponse.Substring(index + 4);
            }
            return null;
        }
        private string formHttpRequest(string url)
        {
            string request = null;
            request += formGetHttpRequestHeader(url);
            return request;
        }
        private string formGetHttpRequestHeader(string url)
        {
            return String.Format("GET {0} HTTP/1.1\r\n", url);
        }
        private string FormHeader(string headerName, string header)
        {
            return String.Format("{0}:{1}\r\n", headerName, header);
        }

        //Convert function
        private string EntryToQuery(Entry entry)
        {
            string query = "?";
            if (entry.prefix != null)
            {
                query += "prefixes=" + string.Join(",", entry.prefix.ToArray()) + "&";
            }
            query += "root=" + entry.root + "&";
            if (entry.suffix != null)
            {
                query += "suffixes=" + string.Join(",", entry.suffix.ToArray());
            }
            return query;
        }

        //Unused shit below
        public bool IsRootExists(string root)
        {
            throw new NotImplementedException();
        }
        public List<string> KnownWordsWithRoot(string root)
        {
            
            throw new NotImplementedException();
        }
        public string RootIfKnown(string word)
        {
            throw new NotImplementedException();
        }
        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
